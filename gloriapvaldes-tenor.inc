\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		\key d \major

		R1*2  |
		g 4 g 8 fis 4 e d 8  |
		e 4. fis 4 r8 d d  |
%% 5
		g 4 g 8 fis 4 e d 8  |
		e 4 d 2 r8 d  |
		e 4 e 8 e 4 e fis 8 ~  |
		fis 2. r8 fis  |
		e 4 e 8 e 4 e d 8 ~  |
%% 10
		d 2. d 8 d  |
		b 4 b 8 a 4 a 8 g fis  |
		g 8 a 2 r8 r d  |
		b 4 b 8 a 4 a 8 g fis  |
		g 8 fis 2 r8 r d  |
%% 15
		cis 4. d e 4  |
		fis 4 ( e 8 ) d 4 r8 r d  |
		b 4 b 8 a 4 g 8 fis fis ~  |
		fis 2 r  |
		g 4 g 8 fis 4 e d 8  |
%% 20
		e 4. fis 4 r8 d d  |
		g 4 g 8 fis 4 e d 8  |
		e 4 d 2 r8 d  |
		e 4 e 8 e 4 e fis 8 ~  |
		fis 2. r8 fis  |
%% 25
		e 4 e 8 e 4 e d 8 ~  |
		d 2. r8 d  |
		b 4 b 8 a 4 a 8 g fis  |
		g 8 a 2 r8 r d  |
		g 4 b 8 a 4 a 8 g fis  |
%% 30
		g 8 fis 2 r8 r d  |
		cis 4. d e 4  |
		fis 4 e 8 d 4 r8 r d  |
		b 4 b 8 a 4 g 8 fis fis ~  |
		fis 2 r  |
%% 35
		g 4 g 8 fis 4 e d 8  |
		e 4. fis 4 r8 d d  |
		g 4 g 8 fis 4 e d 8  |
		e 4 d 2 r8 d  |
		e 4 e 8 e 4 e fis 8 ~  |
%% 40
		fis 2. r8 fis  |
		e 4 e 8 e 4 e d 8 ~  |
		d 2. r4  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Glo -- ria_a Dios en el cie -- lo
		y_en la tie -- rra paz a los hom -- bres
		que a -- ma el Se -- ñor, __
		que a -- ma el Se -- ñor. __

		Por tu_in -- men -- sa glo -- ria te_a -- la -- ba -- mos,
		Te ben -- de -- ci -- mos, te_a -- do -- ra -- mos,
		te glo -- ri -- fi -- ca __ mos.
		Te da -- mos gra -- cias, Se -- ñor. __

		Glo -- ria_a Dios en el cie -- lo
		y_en la tie -- rra paz a los hom -- bres
		que a -- ma el Se -- ñor, __
		que a -- ma el Se -- ñor. __

		Se -- ñor, tú rei -- nas en el cie -- lo,
		Dios Pa -- dre to -- do -- po -- de -- ro -- so,
		Se -- ñor, Hi -- jo ú -- ni -- co,
		Je -- sús, cor -- de -- ro de Dios. __

		Glo -- ria_a Dios en el cie -- lo
		y_en la tie -- rra paz a los hom -- bres
		que a -- ma el Se -- ñor, __
		que a -- ma el Se -- ñor. __
	}
>>
