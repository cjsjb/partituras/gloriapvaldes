\context ChordNames \chords {
	\set chordChanges = ##t

	% intro
	g1 a4.:m d8 ~ d2

	% gloria a dios en el cieelo...
	g4. e8:m ~ e2:m | a4.:m d8 ~ d2 |
	g4. e8:m ~ e2:m | d1 |
	a4.:m c8 ~ c2 | d1 |
	a4.:m a8:7 ~ a2:7 | d1 |

	% por tu inmensa gloria te alabamos...
	g4. e8:m ~ e2:m | a4.:m d8 ~ d2 |
	g4. e8:m ~ e2:m | d1 |
	a1:7 | d1 |
	e4.:m a8:7 ~ a2:7 | d1 |

	% gloria a dios en el cieelo...
	g4. e8:m ~ e2:m | a4.:m d8 ~ d2 |
	g4. e8:m ~ e2:m | d1 |
	a4.:m c8 ~ c2 | d1 |
	a4.:m a8:7 ~ a2:7 | d1 |

	% sennor, tu reinas en el cielo...
	g4. e8:m ~ e2:m | a4.:m d8 ~ d2 |
	g4. e8:m ~ e2:m | d1 |
	a1:7 | d1 |
	e4.:m a8:7 ~ a2:7 | d1 |

	% gloria a dios en el cieelo...
	g4. e8:m ~ e2:m | a4.:m d8 ~ d2 |
	g4. e8:m ~ e2:m | d1 |
	a4.:m c8 ~ c2 | d1 |
	a4.:m a8:7 ~ a2:7 | d1 |
}
