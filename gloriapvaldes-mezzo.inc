\context Staff = "mezzosoprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzosoprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key d \major

		R1*3  |
		e' 4. d' ( a 4 )  |
%% 5
		d' 4. b r4  |
		d' 4. cis' a 4  |
		a 2. r4  |
		d' 4. cis' a 4  |
		a 4. ( b ) cis' 4  |
%% 10
		a 2. d' 8 d'  |
		g' 4 g' 8 fis' 4 fis' 8 e' d'  |
		e' 8 fis' 2 r8 r d'  |
		g' 4 g' 8 fis' 4 fis' 8 e' d'  |
		e' 8 d' 2 r8 r d'  |
%% 15
		cis' 4. d' e' 4  |
		fis' 4 ( e' 8 ) d' 4 r8 r d'  |
		g' 4 g' 8 fis' 4 e' 8 d' d' ~  |
		d' 2 r  |
		R1  |
%% 20
		e' 4. d' ( a 4 )  |
		d' 4. b r4  |
		d' 4. cis' a 4  |
		a 2. r4  |
		d' 4. cis' a 4  |
%% 25
		a 4. ( b ) cis' 4  |
		a 2. r8 d'  |
		g' 4 g' 8 fis' 4 fis' 8 e' d'  |
		e' 8 fis' 2 r8 r d'  |
		g' 4 g' 8 fis' 4 fis' 8 e' d'  |
%% 30
		e' 8 d' 2 r8 r d'  |
		cis' 4. d' e' 4  |
		fis' 4 e' 8 d' 4 r8 r d'  |
		g' 4 g' 8 fis' 4 e' 8 d' d' ~  |
		d' 2 r  |
%% 35
		R1  |
		e' 4. d' ( a 4 )  |
		d' 4. b r4  |
		d' 4. cis' a 4  |
		a 2. r4  |
%% 40
		d' 4. cis' a 4  |
		a 4. ( b ) cis' 4  |
		a 2. r4  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzosoprano" {
		Glo -- ria, __ glo -- ria, glo -- ria a Dios,
		"...a" -- man a el __ Se -- ñor.

		Por tu_in -- men -- sa glo -- ria te_a -- la -- ba -- mos,
		Te ben -- de -- ci -- mos, te_a -- do -- ra -- mos,
		te glo -- ri -- fi -- ca __ mos.
		Te da -- mos gra -- cias, Se -- ñor.

		Glo -- ria, __ glo -- ria, glo -- ria a Dios,
		"...a" -- man a el __ Se -- ñor.

		Se -- ñor, tú rei -- nas en el cie -- lo,
		Dios Pa -- dre to -- do -- po -- de -- ro -- so,
		Se -- ñor, Hi -- jo ú -- ni -- co,
		Je -- sús, cor -- de -- ro de Dios. __

		Glo -- ria, __ glo -- ria, glo -- ria a Dios,
		"...a" -- man a el __ Se -- ñor.
	}
>>
